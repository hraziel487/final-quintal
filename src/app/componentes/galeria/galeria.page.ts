import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImagenModalPage } from '../imagen-modal/imagen-modal.page';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  constructor(private modalCtrl: ModalController, private menu: MenuController) { }

  imagenes = [
    'amigo.jpg',
    'chica.jpg',
    'Dia.jpg',
    'Diagnostico.jpg',
    'Madre.jpg',
    'Niña.jpg',
    'Pata.jpg',
    'Perro.jpg',
  ];
  verImagen(index) {

    this.modalCtrl.create({
      component : ImagenModalPage,
      componentProps : {
        index
      }
    }).then(modal => modal.present());
  }

  ngOnInit() {
  }


}
